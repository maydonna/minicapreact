import React, { useState, Fragment} from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

  const AppNavbar = (props) => {

    return (
        <div className="mb-5">
            <Navbar color="dark"  expand="md">

            <NavbarToggler/>

            <Collapse >
            <Nav className="mr-auto" navbar>

            <NavItem>
            {/*<NavLink href="https://github.com/reactstrap/reactstrap">Tasks</NavLink>*/}

                <Link to="/minicaps" className="nav-link">Minicap</Link>
            </NavItem>

            </Nav>
            
            </Collapse>
        </Navbar>
        </div>
    );
  }

export default AppNavbar;