import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import { Card, CardImg, CardTitle, CardText, CardColumns, CardSubtitle, CardBody } from 'reactstrap';
import { Fade } from 'reactstrap';

const MinicapImgForm = (props) => {
    //state
    const [minicapPic, setMinicapPic] = useState(undefined)

    const onChangeHandler = (e) => {
        setMinicapPic(e.target)
        setMinicapPic(e.target.files[0])
    }

    const onSubmitHandler = (e) => {
        e.preventDefault()
        const formData = new FormData();
        formData.append("upload", minicapPic);
        props.updateImage(formData);
    }
    const [fadeIn1, setFadeIn1] = useState(true);
    const [fadeIn2, setFadeIn2] = useState(true);
    const [fadeIn3, setFadeIn3] = useState(true);
    const [fadeIn4, setFadeIn4] = useState(true);
    const [fadeIn5, setFadeIn5] = useState(true);
    const [fadeIn6, setFadeIn6] = useState(true);

    const toggle1 = () => setFadeIn1(!fadeIn1);
    const toggle2 = () => setFadeIn2(!fadeIn2);
    const toggle3 = () => setFadeIn3(!fadeIn3);
    const toggle4 = () => setFadeIn4(!fadeIn4);
    const toggle5 = () => setFadeIn5(!fadeIn5);
    const toggle6 = () => setFadeIn6(!fadeIn6);

    return (
        <Form className="border p-4 rounded" onSubmit={e => onSubmitHandler(e)}>
            <CardColumns>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle1}>open your fortune</Button>
                                    <Fade in={fadeIn1} tag="h5" className="a mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle2}>open your fortune</Button>
                                    <Fade in={fadeIn2} tag="h5" className="mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle3}>open your fortune</Button>
                                    <Fade in={fadeIn3} tag="h5" className="mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle4}>open your fortune</Button>
                                    <Fade in={fadeIn4} tag="h5" className="mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle5}>open your fortune</Button>
                                    <Fade in={fadeIn5} tag="h5" className="mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
                <Card>
                    <CardBody>
                        <CardTitle>Cookie Image dito or itong button mismo ung image ng cookie magbreak ung cookie or hindi kaya pwede na</CardTitle>
                            <div>
                                <Button color="primary" onClick={toggle6}>open your fortune</Button>
                                    <Fade in={fadeIn6} tag="h5" className="mt-3">
                                        qoutes dito..randomize na later
                                    </Fade>
                            </div>
                    </CardBody>
                </Card>
            </CardColumns>
           
            <img className="img-fluid" src={`http://localhost:4000/members/${props._id}/upload`} alt="minicap pic"/>
            <FormGroup>
                <Input type="file" name="profilePic" onChange={e => onChangeHandler(e)}/>
            </FormGroup>

            <Button className="btn-block border">Save Image</Button>
        </Form>
        )
}

export default MinicapImgForm