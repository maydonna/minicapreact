
import React, { useState } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MinicapPage from './pages/MinicapPage';
const App = () => { 
	const Load = (props, page)=>{
		switch(page) {
			case "MinicapPage": return <MinicapPage {...props} />	
		}
	}
	return (
		<BrowserRouter>
			<AppNavbar/>
			<Switch>	
				<Route exact path="/" render={ (props)=> Load(props, "MinicapPage") }/>
				{/* <Route exact path="/" /> */}
				{/* <Route path="*" render={ (props)=> Load(props, "NotFoundPage") }/> */}
			</Switch>
		</BrowserRouter>
	)
}
export default App
