import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MinicapImgForm from '../forms/MinicapImgForm';
import axios from 'axios';

const MinicapPage = (props) => {
	return (
		<Container>
			<Row>
				<Col>
					<h1>Minincap page</h1>
					<div>
						<Button className="btn-sm border mr-1">
							Sana All
						</Button>
						<Button className="btn-sm border mr-1">
							Happy
						</Button>
						<Button className="btn-sm border mr-1">
							Sawi
						</Button>
						<Button className="btn-sm border mr-1">
							Hoping
						</Button>
						<Button className="btn-sm border mr-1">
							Hopeless
						</Button>
						<Button className="btn-sm border mr-1">
							Chill
						</Button>
						<Button className="btn-sm border mr-1">
							Etc haha!
						</Button>
					</div>
				</Col>
				
			</Row>
			<Row>
				<Col>
					<MinicapImgForm/>
				</Col>
			</Row>
            
		</Container>
		);
}

export default MinicapPage